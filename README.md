# Incubator Submission

Welcome to the GNOME Core and Development app incubator submission.

A submission for incubation is the first step for an app to make its way into GNOME Core or GNOME Development. The complete process is described in [App Lifecycle](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/AppLifecycle.md) document. Before submitting your app, please carefully check the following points:

  - The barrier to entering GNOME Core is usually much higher than entering GNOME Circle. Core apps are not regularly exchanged, and the set of core apps is not extended regularly. Please check if a [GNOME Circle submission](https://gitlab.gnome.org/Teams/Circle) could be a better fit.
  - Submission of a Core app to the incubation phase should usually be based on existing [design mockups](https://gitlab.gnome.org/Teams/Design/app-mockups). If you have an idea for an app not covered by current Design Team proposals, please contact the Design Team first.
  - The potential maintainers of the app should ideally get in touch with the Design and/or Release Team already when beginning the work on this proposal.
  - The app should target to fulfill the [Core App or Development Tool Definition](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/OfficialAppDefinition.md) and the corresponding [App Criteria](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/AppCriteria.md). You should be familiar with all the requirements when submitting an app for incubation.
  - The app submitted for incubation does not need to fulfill all of those criteria yet. The requirement for incubation is a minimal viable product (MVP) that can be worked on during the incubation stage.

If you have any questions feel free to contact the Design or Release Team.

  - **[Submit your app](https://gitlab.gnome.org/Incubator/Submission/-/issues/new)**
