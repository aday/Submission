<!-- Issue title: <codename> (<generic name>) [Example: Loupe (Image Viewer)] -->

- Project type (Core or Development): …
- Project codename (not generic name): …
- Current repository location: …

##### Links to design mockups and relevant issues

<!-- 
    Issues or sources discussing this app (if existent)
-->

##### Dependencies not included in GNOME runtime

##### Installation/build instructions

<!--
    For reviewers and stakeholders to be able to try your app.
-->

##### Planned features

The following features are not implemented yet but are planned to be
added until inclusion into Core/Development:

- [ ] …

##### More information

<!--
    Potentially more information on why this app should be included in
    GNOME Core apps or GNOME Development tools.
-->

#### App replacement

<!--
    Please remove this section if the submission is not intended to
    replace an existing Core app or Development tool
-->

- Apps that will be replaced: …

##### Abandoned features

With the app replacement, the following features that are currently
available in Core, will be abandoned:

- …
